<?php
/**
 * @file
 * The actiual component hooks
 */

/**
 * Implements hook_webform_defaults_component().
 */
function _webform_defaults_imagepicker() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'images' => array(),
      'analysis' => TRUE,
    ),
  );
}

/**
 * Implements hook_webform_theme_component().
 */
function _webform_theme_imagepicker() {
  return array(
    'webform_display_imagepicker' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements hook_webform_edit_component().
 */
function _webform_edit_imagepicker($component) {
  $form = webform_imagepicker_component_form($component);
  return $form;
}


/**
 * Implements hook_webform_render_component().
 */
function _webform_render_imagepicker($component, $value = NULL, $filter = TRUE) {
  drupal_add_js(drupal_get_path('module', 'webform_imagepicker') . '/js/webform_imagepicker.js', 'file');
  drupal_add_css(drupal_get_path('module', 'webform_imagepicker') . '/css/webform_imagepicker.css', 'file');
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;
  $element = array(
    '#type' => 'hidden',
    '#default_value' => $filter ? _webform_filter_values($component['value'], $node, NULL, NULL, FALSE) : $component['value'],
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#prefix' => '<div class="imagepicker"><div class="imagepicker-images">' . _webform_render_imagepicker_images($component) . '</div><div class="imagepicker_value_field">',
    '#suffix' => '</div></div>',
    '#theme_wrappers' => array('webform_element'),
  );
  if (isset($value)) {
    $element['#default_value'] = $value[0];
  }
  else {
    $element['#default_value'] = '';
  }
  return $element;
}


/**
 * Render images.
 */
function _webform_render_imagepicker_images($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $imgs = '';
  $images = _webform_imagepicker_images($component);
  foreach ($images as $img) {
    $imgs .= $img->output;
  }
  return '<label>' . $component['name'] . '</label>' . $imgs;
}


/**
 * Implements hook_webform_display_component().
 */
function _webform_display_imagepicker($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#image_style' => $component['extra']['images']['style'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_imagepicker',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#value' => $value[0],
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
    '#component' => $component,
  );
}

/**
 * Implements hook_webform_table_component().
 */
function _webform_table_imagepicker($component, $value) {
  $fid = check_plain(empty($value[0]) ? '' : $value[0]);
  if (!empty($fid)) {
    $file = file_load($fid);
    if (is_object($file)) {
      return $file->filename;
    }
  }
  return '';
}

/**
 * Implements hook_webform_headers_component().
 */
function _webform_csv_headers_imagepicker($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = $component['name'];
  return $header;
}

/**
 * Implements hook_webform_csv_data_component().
 */
function _webform_csv_data_imagepicker($component, $export_options, $value) {
  $fid = check_plain(empty($value[0]) ? '' : $value[0]);
  if (!empty($fid)) {
    $file = file_load($fid);
    if (is_object($file)) {
      return $file->filename;
    }
  }
  return '';
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_imagepicker($component, $sids = array(), $single = FALSE, $join = NULL) {
  $images = _webform_imagepicker_images($component);
  // Create a generic query for the component.
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->condition('wsd.nid', $component['nid'])
    ->condition('wsd.cid', $component['cid'])
    ->condition('wsd.data', '', '<>');

  if ($sids) {
    $query->condition('wsd.sid', $sids, 'IN');
  }

  if ($join) {
    $query->innerJoin($join, 'ws2_', 'wsd.sid = ws2_.sid');
  }

  $rows = array();
  $other = array();
  $normal_count = 0;
  $options_fids = array();
  foreach ($images as $img) {
    $options[] = $img->file->fid;
    $options_fids[$img->file->fid] = $img->file;
  }

  if ($options) {
    // Gather the normal results first (not "other" options).
    $query->addExpression('COUNT(wsd.data)', 'datacount');
    $result = $query
      ->condition('wsd.data', $options, 'IN')
      ->fields('wsd', array('data'))
      ->groupBy('wsd.data')
      ->execute();
    foreach ($result as $data) {
      if (isset($data['data'])) {
        $display_option = $options_fids[$data['data']]->filename;
        $rows[$data['data']] = array(webform_filter_xss($display_option), $data['datacount']);
        $normal_count += $data['datacount'];
      }
    }
  }

  return array(
    'table_rows' => $rows,
    'other_data' => $other,
  );
}

/**
 * Helper function returns list of image files.
 */
function _webform_imagepicker_images($component) {
  $files = file_load_multiple(array_values($component['extra']['images']['images_fids']));
  $images = array();
  foreach ($files as $f) {
    $img = '<div class="webform_imagepicker_choice_wrapper">' . theme('image_style', array(
      'style_name' => $component['extra']['images']['style'],
      'path' => $f->uri,
      'attributes' => array(
        'rel' => $f->filename,
        'fid' => $f->fid,
        'class' => 'webform_imagepicker_choice ',
      ),
      )
    ) . '</div>';
    $new_img = new stdClass();
    $new_img->file = $f;
    $new_img->output = $img;
    $images[] = $new_img;
  }
  return $images;
}
