/**
 * @file
 * JavaScript behaviors for the front-end display of webforms.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.webform_imagepicker = Drupal.behaviors.webform_imagepicker || {};
  Drupal.behaviors.webform_imagepicker.attach = function (context) {
    $.each($('.imagepicker'),function(index,item){
      var $imagepicker = $(item);
      $imagepicker.find('.imagepicker-images .webform_imagepicker_choice').click(function(){
        var $this = $(this);
        $imagepicker.find('.webform_imagepicker_choice_wrapper').removeClass('active');
        $this.parents('.webform_imagepicker_choice_wrapper').addClass('active');
        $imagepicker.find('.imagepicker_value_field input').val($this.attr('fid'));
      });
    });
  };

  Drupal.webform_imagepicker = Drupal.webform_imagepicker || {};


})(jQuery);
